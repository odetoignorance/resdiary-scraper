<?php

namespace OdeToIgnorance\Scraper\Exception;

class FileCreationFailedException extends \LogicException
{
}
