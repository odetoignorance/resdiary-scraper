<?php

namespace OdeToIgnorance\Scraper\Helper;

use Goutte\Client;
use InvalidArgumentException;
use OdeToIgnorance\Scraper\Exception\FileCreationFailedException;
use Symfony\Component\DomCrawler\Crawler;

class Scraper
{
    const URL = 'http://www.black-ink.org';
    const SAVE_LOCATION = __DIR__ . '/../Asset/Output/';

    /**
     * @var Client $client
     */
    protected $client;

    /**
     * @var array $output
     */
    protected $output = [];

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Performs a scrape of given category and fetches link information (url, title, description, keywords, file size)
     * Sets the results to $output class variable.
     * Use the class getters to return the output in the desired supported format.
     * Returns the class instance to allow for chaining of the output getters
     *
     * @param string $category
     * @return Scraper
     */
    public function scrape($category)
    {
        if ((empty($category)) || !is_string($category)) {
            throw new InvalidArgumentException('Please provide a category to search for in your scrape');
        }

        $crawler = $this->client->request('GET', self::URL);
        print_r($crawler);
        $nodes = $this->filterNodesByCategory($crawler, $category);

        $this->output['links'] = $this->flattern(
            ['posts' => $nodes->each(function($node) {
                return ['links' => $node->filter('li > a')->each(function ($link) {
                    $linkUrl = $link->filter('a')->attr('href');
                    $metaTags = $this->fetchMetaTags($linkUrl);

                    return [
                        'url' => $linkUrl,
                        'title' => $link->filter('a')->text(),
                        'meta_description' => (isset($metaTags['keywords'])) ? $metaTags['keywords'] : '',
                        'keywords' => (isset($metaTags['keywords'])) ? $metaTags['keywords'] : '',
                        'file-size' => (int) $this->fetchContentLength($linkUrl),
                    ];
                })];
            })]
        );

        $this->output['total-size'] = $this->calculateTotalFileSize($this->output['links']);

        return $this;
    }

    /**
     * Getter for $output, bound as a json encoded string and suitable for plain text saving
     *
     * @return string
     */
    public function getJsonOutput()
    {
        return json_encode($this->output, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }

    /**
     * Getter for $output, as a php array
     *
     * @return array
     */
    public function getArrayOutput()
    {
        return $this->output;
    }

    /**
     * Save the output to a json file
     *
     * @throws FileCreationFailedException if the save file could not be created
     */
    public function saveOutputFile() {
        $file = fopen(self::SAVE_LOCATION.date('Ymd-His').'-scrape.json', 'w');

        if ($file === false) {
            throw new FileCreationFailedException(
                sprintf(
                    'Could not create file in location %s, please ensure you can write to this directory',
                    self::SAVE_LOCATION
                )
            );
        }

        fwrite($file, $this->getJsonOutput());
        fclose($file);
    }

    /**
     * Calculates the total file size of all the links
     *
     * @param array $links
     * @return int
     */
    protected function calculateTotalFileSize(array $links)
    {
        $totalSize = 0;

        foreach ($links as $link) {
            $totalSize += $link['file-size'];
        }

        return (int) $totalSize;
    }

    /**
     * Fetches the meta tags of the link.
     * Currently only supports meta name tags
     *
     * TODO Build in support for Facebook OpenGraph, and Twitter meta tags
     *
     * @param string $linkUrl
     * @return array
     */
    protected function fetchMetaTags($linkUrl)
    {
        return get_meta_tags($linkUrl);
    }

    /**
     * Fetches the content length of the link from its header
     * Due to some headers returning an array of content lengths, some extra processing is required
     *
     * @param string $linkUrl
     * @return int
     */
    protected function fetchContentLength($linkUrl)
    {
        $headers = get_headers($linkUrl, true);

        if (isset($headers['Content-Length'])) {
            $contentLength = $headers['Content-Length'];
        } else if (isset($headers['content-length'])) {
            $contentLength = $headers['content-length'];
        } else {
            $contentLength = 0;
        }

        if (is_array($contentLength)) {
            $biggestLength = 0;

            foreach ($contentLength as $length) {
                $biggestLength = ($biggestLength > $length) ? $biggestLength : $length;
            }

            $contentLength = $biggestLength;
        }

        return (int) $contentLength;
    }

    /**
     * Flatterns the 2-level array in to one, for nicer json encoding.
     *
     * @param array $array
     * @return array
     */
    protected function flattern(array $array)
    {
        $flatArray = [];

        foreach ($array['posts'] as $post) {
            foreach ($post['links'] as $link) {
                $flatArray[] = $link;
            }
        }

        return $flatArray;
    }

    /**
     * Filters the crawler nodes by category, and removes any nodes that do not match the filter
     * Currently filtering on CSS selectors
     *
     * @param Crawler $crawler
     * @param string $category
     * @return Crawler
     */
    protected function filterNodesByCategory(Crawler $crawler, $category)
    {
        return $crawler->filter("main > article > div")->reduce(function ($node) use ($category) {
            if ($node->filter('footer > div > span > a')->text() == $category) {
                return true;
            };

            return false;
        });
    }
}
