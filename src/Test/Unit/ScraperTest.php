<?php

namespace OdeToIgnorance\Scraper\Test\Unit;

use OdeToIgnorance\Scraper\Helper\Scraper;
use PHPUnit_Framework_TestCase;

class ScraperTest extends PHPUnit_Framework_TestCase
{
    protected $scraper;

    public function setUp()
    {
        $crawlerMockObject = $this->getMockBuilder('\Symfony\Component\DomCrawler\Crawler')
            ->disableOriginalConstructor()
            ->getMock();

        $clientMockObject = $this->getMockBuilder('\Goutte\Client')
            ->setMethods(null)
            ->getMock();

        $clientMockObject->method('request')
            ->will($this->returnValue($crawlerMockObject));

        $this->scraper = new Scraper($clientMockObject);
    }

    public function testConstructer()
    {
        $this->assertInstanceOf('\OdeToIgnorance\Scraper\Helper\Scraper', $this->scraper);
    }

    public function testScrapeThrowsExceptionWithEmptyCategory()
    {
        $this->setExpectedException('\InvalidArgumentException');
        $this->scraper->scrape('');
    }

    public function testScrapeThrowsExceptionWithArrayOfCategories()
    {
        $this->setExpectedException('\InvalidArgumentException');
        $this->scraper->scrape(['Category1', 'Category2']);
    }
}
