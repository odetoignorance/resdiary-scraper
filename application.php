<?php

require __DIR__.'/vendor/autoload.php';

use Goutte\Client;
use OdeToIgnorance\Scraper\Exception\FileCreationFailedException;
use OdeToIgnorance\Scraper\Helper\Scraper;

try {
    $scraper = new Scraper(new Client);
    echo $scraper->scrape("Digitalia")->getJsonOutput();
    $scraper->saveOutputFile();
} catch (FileCreationFailedException $e) {
    echo $e->getMessage();
} catch (InvalidArgumentException $e) {
    echo $e->getMessage();
} catch (Exception $e) {
    echo 'An unknown error has occured';
}